// Запасы на складе
var apples = 20,
    strawberry = 20,
    apricot = 20,
    flour = 20,
    milk = 20,
    eggs = 50;

// Печем яблочный пирог
function applePie (applePies) {

  for (var applePie = 1; applePie <= applePies; applePie++) {

    if (apples - 3 < 0) {
      console.log('Кончились яблоки! Купили еще 10 кг')
      apples = apples + 10;
    } else if (flour - 2 < 0) {
      console.log('Кончилась мука! Купили еще 10 кг')
      flour = flour + 10;
    } else if (milk - 1 < 0) {
      console.log('Кончилось молоко! Купили еще 10 литров')
      milk = milk + 10;
    } else if (eggs - 3 < 0) {
      console.log('Кончились яйца! Купили еще 10 штук')
      eggs = eggs + 10;
    } else {
    apples = apples - 3;
    flour = flour - 2;
    milk = milk - 1;
    eggs = eggs - 3;
    }

    console.log('Яблочный пирог номер %d готов!', applePie);
  }
}

// Печем клубничный пирог
function strawPie (strawPies) {

  for (var strawPie = 1; strawPie <= strawPies; strawPie++) {

    if (strawberry - 5 < 0) {
	  	console.log('Кончилась клубника! Купили еще 10 кг')
	  	strawberry = strawberry + 10;
	  } else if (flour - 2 < 0) {
	  	console.log('Кончилась мука! Купили еще 10 кг')
	  	flour = flour + 10;
	  } else if (milk - 1 < 0) {
	  	console.log('Кончилось молоко! Купили еще 10 литров')
	  	milk = milk + 10;
	  } else if (eggs - 3 < 0) {
	  	console.log('Кончились яйца! Купили еще 10 штук')
	  	eggs = eggs + 10;
	  } else {
	  strawberry = strawberry - 5;
	  flour = flour - 1;
	  milk = milk - 2;
	  eggs = eggs - 4;
	  }

    console.log('Клубничный пирог номер %d готов!', strawPie);
  }
}


// Печем абрикосовый пирог
function apricotPie (apricotPies) {

  for (var apricotPie = 1; apricotPie <= apricotPies; apricotPie++) {

    if (apricot - 2 < 0) {
      console.log('Кончились абрикосы! Купили еще 10 кг')
      apricot = apricot + 10;
    } else if (flour - 2 < 0) {
      console.log('Кончилась мука! Купили еще 10 кг')
      flour = flour + 10;
    } else if (milk - 1 < 0) {
      console.log('Кончилось молоко! Купили еще 10 литров')
      milk = milk + 10;
    } else if (eggs - 3 < 0) {
      console.log('Кончились яйца! Купили еще 10 штук')
      eggs = eggs + 10;
    } else {
    apricot = apricot - 2;
    flour = flour - 3;
    milk = milk - 2;
    eggs = eggs - 2;
    }

    console.log('Абрикосовый пирог номер %d готов!', apricotPie);
  }
}

applePie (10);
strawPie (10);
apricotPie (10);
