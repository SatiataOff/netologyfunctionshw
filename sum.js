// с рекурсией
function sum(num) {
  if (num != 0) {
    return num + sum (num - 1);
  } else {
    return num;
  }
}

console.log(sum(3));

// с циклом
function sumFunc(num) {
  var sum = 0;
  for (var i = 1; i <= num; i++) {
    sum += i;
  }
  return sum;
}

console.log(sumFunc(3));

// По-моему, лучше способ с циклом, т.к. код более читаемый и понятный
